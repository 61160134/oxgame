import java.util.*;

public class OXgame {
    public static Scanner kb = new Scanner(System.in);
    public static char[][] OXgame = {{' ',' ',' '},{' ',' ',' '},{' ',' ',' '}};
    public static char turn = 'O';
    public static int count = 0 ;
    public static int row;
    public static int col;
    
    public static void main(String[] args) {
        funcWelcome();
        
        for(;;){
        	funcTable();
        	funcTurn();
        	funcInput();
            if(showDraw()==true){
                break;
            }else if(checkWin()==true){
                break;
            }
            turnCheck();
        }    
    }
    
    
    public static void funcWelcome() {
        System.out.println(">>>>>>>>> WELCOME TO OX GAME <<<<<<<<");
    }
    
 
    public static void funcTable() {
        for (int i = 0; i < 3; i++) {
            System.out.println("++++++++++++++");
            for (int j = 0; j < 3; j++) {
            	System.out.print("| ");
            	System.out.print(OXgame[i][j]+ " ");
            }
            System.out.print("|");
            System.out.println();
	}
        System.out.println("++++++++++++++");
    }
    
    
    public static void funcInput() {
            System.out.println("Please input row and collum : ");
            row = kb.nextInt();
            col = kb.nextInt();
            count++;     
            if(funcError()==true){
            	funcInput();
            }else {
                OXgame[row][col] = turn;
            }
    }
    
    
    private static void funcTurn() {
        System.out.println("Turn : "+turn);
    }
    
    
    public static void turnCheck(){
        if(turn == 'O'){
            turn = 'X';
        }else if(turn == 'X'){
            turn = 'O';
        }
    }
    
    
    public static boolean funcError() {
        if (row > 2 || row < 0) {
            System.out.println("ERROR , Please input 0,1,2");
            return true;
	    }else if (col > 2 || col < 0) {
            System.out.println("ERROR , Please input 0,1,2");
            return true;
	    }else if (OXgame[row][col]== 'O'||OXgame[row][col]== 'X') {
            System.out.println("Please choose another Row and Collum!!");
            return true;
        }else{
            return false;
        }
    }
    
    
    public static boolean checkWin() {
        for(int r = 0 ;r < 3 ;r++){
            if (OXgame[r][0]==turn&&OXgame[r][1]==turn&&OXgame[r][2]==turn){
                showWin();
                return true;
            }else if(OXgame[0][r]==turn&&OXgame[1][r]==turn&&OXgame[2][r]==turn){
                showWin();
                return true;
            }else if(OXgame[0][0]==turn&&OXgame[1][1]==turn&&OXgame[2][2]==turn){
                showWin();
                return true;
            }else if(OXgame[0][2]==turn&&OXgame[1][1]==turn&&OXgame[2][0]==turn){
                showWin();
                return true;
            }
        }return false;
    }
    
    
    public static void showWin() {
    	System.out.println(">>>>>>>>>> Game Over <<<<<<<<<<");
    	for (int i = 0; i < 3; i++) {
            System.out.println("++++++++++++++");
            for (int j = 0; j < 3; j++) {
            	System.out.print("| ");
            	System.out.print(OXgame[i][j]+ " ");
            }
            System.out.print("|");
            System.out.println();
	}
        System.out.println("++++++++++++++");
        System.out.println(">>>>>>> Congratulations <<<<<<<");
        System.out.println("Player "+turn+" Win");
    }
    
       
    public static boolean showDraw() {
        if(count == 9){
        	for (int i = 0; i < 3; i++) {
                System.out.println("++++++++++++++");
                for (int j = 0; j < 3; j++) {
                	System.out.print("| ");
                	System.out.print(OXgame[i][j]+ " ");
                }
                System.out.print("|");
                System.out.println();
    	}
            System.out.println("++++++++++++++");
            System.out.println(">>>> Draw <<<<");
            return true;
        }return false;
    }
    
}